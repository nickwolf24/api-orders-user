require 'test_helper'

class StateOrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @state_order = state_orders(:one)
  end

  test "should get index" do
    get state_orders_url, as: :json
    assert_response :success
  end

  test "should create state_order" do
    assert_difference('StateOrder.count') do
      post state_orders_url, params: { state_order: { name: @state_order.name } }, as: :json
    end

    assert_response 201
  end

  test "should show state_order" do
    get state_order_url(@state_order), as: :json
    assert_response :success
  end

  test "should update state_order" do
    patch state_order_url(@state_order), params: { state_order: { name: @state_order.name } }, as: :json
    assert_response 200
  end

  test "should destroy state_order" do
    assert_difference('StateOrder.count', -1) do
      delete state_order_url(@state_order), as: :json
    end

    assert_response 204
  end
end

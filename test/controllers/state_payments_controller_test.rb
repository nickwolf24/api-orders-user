require 'test_helper'

class StatePaymentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @state_payment = state_payments(:one)
  end

  test "should get index" do
    get state_payments_url, as: :json
    assert_response :success
  end

  test "should create state_payment" do
    assert_difference('StatePayment.count') do
      post state_payments_url, params: { state_payment: { name: @state_payment.name } }, as: :json
    end

    assert_response 201
  end

  test "should show state_payment" do
    get state_payment_url(@state_payment), as: :json
    assert_response :success
  end

  test "should update state_payment" do
    patch state_payment_url(@state_payment), params: { state_payment: { name: @state_payment.name } }, as: :json
    assert_response 200
  end

  test "should destroy state_payment" do
    assert_difference('StatePayment.count', -1) do
      delete state_payment_url(@state_payment), as: :json
    end

    assert_response 204
  end
end

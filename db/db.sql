BEGIN;
INSERT INTO public.state_payments
(name, created_at, updated_at)
VALUES('pagada', now(), now());

INSERT INTO public.state_payments
(name, created_at, updated_at)
VALUES('no pagada', now(), now());

INSERT INTO public.state_payments
(name, created_at, updated_at)
VALUES('pendiente de pago', now(), now());


INSERT INTO public.state_orders
("name", created_at, updated_at)
VALUES('recibida', now(), now());

INSERT INTO public.state_orders
("name", created_at, updated_at)
VALUES('en preparación', now(), now());


INSERT INTO public.state_orders
("name", created_at, updated_at)
VALUES('en reparto', now(), now());


INSERT INTO public.state_orders
("name", created_at, updated_at)
VALUES('entregada', now(), now());

-- PARA EL USUARIO, TE RECOMIEDO CREARLO POR LA API, DADO QUE LA CLAVE PASA POR UN PROCESO DE ENCRIPTACIÓN 

-- INSERT INTO public.users
-- ("name", email, password_digest, created_at, updated_at)
-- VALUES('pablo', 'pablo@mail.com', 'mypassword', now(), now());


-- INSERT INTO public.users
-- ("name", email, password_digest, created_at, updated_at)
-- VALUES('nicolas', 'nicolas@mail.com', 'mypassword', now(), now());

COMMIT;
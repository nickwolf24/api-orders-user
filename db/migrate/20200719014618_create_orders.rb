class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.integer :mount
      t.datetime :payment_date
      t.datetime :deliver_date
      t.integer :user_id
      t.string :state_payment_id
      t.string :state_order_id

      t.timestamps
    end
  end
end

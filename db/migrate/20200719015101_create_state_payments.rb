class CreateStatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :state_payments do |t|
      t.string :name

      t.timestamps
    end
  end
end

Rails.application.routes.draw do
  post 'user_token' => 'user_token#create'
  resources :users
  # resources :auth
  resources :state_orders
  resources :state_payments
  resources :orders
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

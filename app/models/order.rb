class Order < ApplicationRecord
    belongs_to :user
    belongs_to :state_order
    belongs_to :state_payment
    
    validate :add_deliver_date, :add_payment_date
    
    validates :mount, :presence => true
    validates :state_payment_id, :presence => true
    validates :state_order_id, :presence => true
    validates :payment_date, :presence => false
    validates :deliver_date, :presence => false
    validates :user_id, :presence => true

    def response_order(id)
        Order.find_by_sql(["SELECT orders.*, state_payments.name, state_orders.name, users.name, users.email FROM orders INNER JOIN users ON users.id = orders.user_id INNER JOIN state_payments ON state_payments.id = orders.state_payment_id::numeric INNER JOIN state_orders ON state_orders.id = orders.state_order_id::numeric  WHERE (orders.id = ?)", id])
    end

    private
    def user_exist
        unless self.user_id == nil
            if User.find(self.user_id).present?
                p 'user exist'
            else 
                p 'user not found'
            end 
        end
    end

    def add_deliver_date
        unless self.state_order_id == nil
            if StateOrder.where( ["id = ? AND name = ?", self.state_order_id, 'entregada'] ).present?
                self.deliver_date = Time.zone.now
                puts 'Agregando fecha de entrega a la orden'
            else
                self.deliver_date = nil
            end
        end
    end

    def add_payment_date
        unless self.state_payment_id == nil
            if StatePayment.where( ["id = ? AND name = ?", self.state_payment_id, 'pagada'] ).present?
                self.payment_date = Time.zone.now
                puts 'Agregando fecha de pago a la orden'
            else 
                self.payment_date = nil
            end
        end
    end
end
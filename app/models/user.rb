class User < ApplicationRecord
    has_secure_password

    has_many :orders, :dependent => :destroy
    validates_associated :orders
    

    # attr_accessor :name, :email
    
    validates :name,  :presence => true
    # validates :password, :presence => true
    
    validates :email, uniqueness: true,  :presence => true,
                        :format => {
                            :with => URI::MailTo::EMAIL_REGEXP ,
                            :message => 'Error - Email con formato incorrecto'
                        }
    
    def to_token_payload
        {
            sub: id,
            email: email
        }
    end
end

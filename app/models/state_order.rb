class StateOrder < ApplicationRecord
    has_many :orders, :dependent => :destroy
    validates_associated :orders
    validates :name, :presence => true, :uniqueness => true
end

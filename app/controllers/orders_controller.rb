class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :update, :destroy]
  before_action :authenticate_user, only: [:show, :index, :create]

  # GET /orders
  def index
    @orders = Order.find_by_sql(response_index_query)
    render json: @orders
  end

  # GET /orders/1
  def show
    render json: @order.response_order(params[:id])
  end

  # POST /orders
  def create
    @order = Order.new(order_params)
    if @order.save
      render json: @order, status: :created, location: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /orders/1
  def update
    if @order.update(params_put)
      render json: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /orders/1
  def destroy
    @order.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def order_params
      params.require(:order).permit(:mount, :payment_date, :deliver_date, :user_id, :state_payment_id, :state_order_id)
    end

    def params_put
      params.require(:order).permit(:state_payment_id, :state_order_id)
    end

    def response_index_query
      ["SELECT orders.*, state_payments.name, state_orders.name, users.name, users.email FROM orders INNER JOIN users ON users.id = orders.user_id INNER JOIN state_payments ON state_payments.id = orders.state_payment_id::numeric INNER JOIN state_orders ON state_orders.id = orders.state_order_id::numeric ORDER BY orders.id ASC"]
    end
end

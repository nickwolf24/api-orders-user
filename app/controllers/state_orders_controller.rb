class StateOrdersController < ApplicationController
  before_action :set_state_order, only: [:show, :update, :destroy]
  before_action :authenticate_user, only: [:show, :index, :create]

  # GET /state_orders
  def index
    @state_orders = StateOrder.all

    render json: @state_orders
  end

  # GET /state_orders/1
  def show
    render json: @state_order
  end

  # POST /state_orders
  def create
    @state_order = StateOrder.new(state_order_params)

    if @state_order.save
      render json: @state_order, status: :created, location: @state_order
    else
      render json: @state_order.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /state_orders/1
  def update
    if @state_order.update(state_order_params)
      render json: @state_order
    else
      render json: @state_order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /state_orders/1
  def destroy
    @state_order.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_state_order
      @state_order = StateOrder.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def state_order_params
      params.require(:state_order).permit(:name)
    end
end

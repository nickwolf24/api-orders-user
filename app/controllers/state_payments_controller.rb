class StatePaymentsController < ApplicationController
  before_action :set_state_payment, only: [:show, :update, :destroy]
  before_action :authenticate_user, only: [:show, :index, :create]

  # GET /state_payments
  def index
    @state_payments = StatePayment.all

    render json: @state_payments
  end

  # GET /state_payments/1
  def show
    render json: @state_payment
  end

  # POST /state_payments
  def create
    @state_payment = StatePayment.new(state_payment_params)
    if @state_payment.save
      render json: @state_payment, status: :created, location: @state_payment
    else
      render json: @state_payment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /state_payments/1
  def update
    if @state_payment.update(state_payment_params)
      render json: @state_payment
    else
      render json: @state_payment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /state_payments/1
  def destroy
    @state_payment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_state_payment
      @state_payment = StatePayment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def state_payment_params
      params.require(:state_payment).permit(:name)
    end
end
